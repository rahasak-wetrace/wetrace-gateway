FROM golang:1.9

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install dependencies
#RUN go get gopkg.in/mgo.v2
RUN	go get github.com/gorilla/mux
RUN go get github.com/gorilla/handlers
#RUN go get github.com/Shopify/sarama
#RUN go get github.com/wvanbergen/kafka/consumergroup

# copy app
ADD . /app
WORKDIR /app

# build
RUN go build -o build/gateway src/*.go

# server running port
EXPOSE 8751

# .keys volume
VOLUME ["/app/.keys"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]
